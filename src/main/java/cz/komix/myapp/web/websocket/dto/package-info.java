/**
 * Data Access Objects used by WebSocket services.
 */
package cz.komix.myapp.web.websocket.dto;
