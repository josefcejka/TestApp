/**
 * View Models used by Spring MVC REST controllers.
 */
package cz.komix.myapp.web.rest.vm;
